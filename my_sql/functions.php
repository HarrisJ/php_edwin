<?php
include "db.php";

function createData(){
    global $connect;
    $username = $_POST['username'];
    $password = $_POST['password'];
    
    //escape string with symbols
    $username = mysqli_real_escape_string($connect, $username);
    $password = mysqli_real_escape_string($connect, $password);
    
    $hashFormat = "$2y$10$";
    $salt = "iusesomecrazystrings22";
    $combination = $hashFormat . $salt;
    
    $password = crypt($password, $combination); //ecnrypt password mix password with salt
    
/*    if($username && $password){
        
        echo $username."<br>";
        echo $password;
    }else{
        
        echo "value accepted";
    } */ 
    
    $query = "INSERT INTO users(username, password)";
    $query .= "VALUES('$username', '$password')";
    
    $result = mysqli_query($connect, $query);
    
    if(!$result){
        die('query failed' . mysqli_error());
    }else{
        echo "Record Created";
    }
    
}
function showData(){
 
    global $connect; //compulsory to add
    $query = "SELECT * FROM users";
    $result = mysqli_query($connect, $query);
    
    if(!$result){
        die('query failed' . mysqli_error());
    }
    
    while($row = mysqli_fetch_assoc($result)){ //fetch all data from query
        $id = $row['id']; //only want id from the row array        
        echo "<option value='$id'>$id</option>";
                    
    }
                   
}

function updateData(){
        
    global $connect; //compulsory to add
    $username = $_POST['username'];
    $password = $_POST['password'];
    $id = $_POST['id'];
    
    $query = "UPDATE users SET ";
    $query .= "username = '$username', ";
    $query .= "password = '$password' ";
    $query .= "WHERE id = $id ";
    
    $result = mysqli_query($connect, $query);
    if(!$result){
        die("QUERY failed" . mysqli_error($connect) );
    }else{
        echo "Data sucessfully update";
    }
    
}

function deleteRow(){
 
    global $connect; //compulsory to add
    $id = $_POST['id'];
    
    $query = "DELETE FROM users ";
    $query .= "WHERE id = $id ";
    
    $result = mysqli_query($connect, $query);
    if(!$result){
        die("QUERY failed" . mysqli_error($connect) );
    }else{
        echo "record deleted";
    }
                   
}

function readData(){
    
    global $connect;
    global $result;
    $query = "SELECT * FROM users";

    $result = mysqli_query($connect, $query);
    
    if(!$result){
        die('query failed' . mysqli_error());
    }
      
    while($row = mysqli_fetch_row($result)){
    print_r($row);
    }    
}
