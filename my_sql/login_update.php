<?php
include "db.php";
include "functions.php";

if(isset($_POST['submit'])){
    
    updateData();    
}

include "includes/header.php";
include "includes/footer.php";

?>

<div class="container">
    <div class="offset-3 col-sm-6">
    <h1 class="text-center">Update</h1>
    <form action="login_update.php" method="post">
           <div class="form-group"> 
              <label for="username">Username</label>
               <input type="text" name="username" class="form-control">
           </div>
           <div class="form-group">
                <label for="password">Password</label>
               <input type="password" name="password" class="form-control">
           </div>
           <div class="form-group">
               <select name="id" id="">
                <?php
                    showData();       
                
                ?>
               </select>
           </div>
            <input class="btn btn-primary" type="submit" name="submit" value="Update">
        </form>

    </div>
</div>
