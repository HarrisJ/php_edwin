<?php
include "db.php";
include "functions.php";

if(isset($_POST['submit'])){
    
    deleteRow();    
}

include "includes/header.php";
include "includes/footer.php";


?>
<div class="container">
    <div class="offset-3 col-sm-6">
    <h1 class="text-center">Delete</h1>
    <form action="login_delete.php" method="post">
           <div class="form-group">
               <select name="id" id="">
                <?php
                    showData();       
                
                ?>
               </select>
           </div>
            <input class="btn btn-primary" type="submit" name="submit" value="Delete">
        </form>

    </div>
</div>
